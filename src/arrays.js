'use strict'

var colors = ['red', 'blue', 'green']

// Old es5 way to loop
for(var i = 0; i < colors.length; i++) {
    console.log(colors[i]);
}

// ES6 for-of loop
for(let color of colors) {
    console.log(color);
}

/*
 * forEach
 */
colors.forEach(function(color) {
    console.log(color);
});


var items = colors.map(function(ele) {
    return ele + 'h'
})


var numbers = [1,2,3,4,5];
var sum = 0;

numbers.forEach((number) => {
    sum += number;
})
console.log(sum)


/*
 * Reduce
 */
var sum = numbers.reduce((acc, number) => {
    return acc += number;
}, 0)

console.log(sum)

/*
 * Map
 */
var numbers = [1,2,3];
var doubled = numbers.map((number) => {
    return number * 2;
})

doubled

/*
 * filter
 */
var products = [
    {  name:  'cucumber',  type:  'vegetable',  quantity:  0,   price:  1},
    {  name:  'banana',    type:  'fruit',      quantity:  10,  price:  15   },
    {  name:  'celery',    type:  'vegetable',  quantity:  30,  price:  13   },
    {  name:  'orange',    type:  'fruit',      quantity:  3,   price:  5    }
]

var isFruit = (food) => { return food.type === 'fruit' }
var isVegetable = (food) => { return food.type === 'vegetable' }
var isQuantityGreaterThen = (food, price) => { return food.price > price }

var newProducts = products.filter((product) =>
    isVegetable(product) && isQuantityGreaterThen(product, 0)
);

console.log(newProducts)

var post = { id: 4, title: 'New Post' }
var comments = [
    { postId: 41324,  content: 'awesome post' },
    { postId: 3,      content: 'it is ok' },
    { postId: 4,      content: 'neat' }
]

var commentsForPost = (post, comments) => {
  return comments.filter((comment) => {
    return comment.postId === post.id;
  });
}

commentsForPost(post, comments);


/*
 * find
 */
var users = [
  { name: 'Jill' },
  { name: 'Alex' },
  { name: 'Bill' },
];

users.find((user) => {
  return user.name === 'Alex';
});

var Car = function(model) {
  this.model = model;
}

var cars = [
  new Car('Buick'),
  new Car('Camaro'),
  new Car('Focus')
]

cars.find((car) => {
  return car.model == 'Focus';
});

var posts = [
  {id: 1, title: 'new post'},
  {id: 2, title: 'old post'}
]

var comment = { postId: 1, content: 'Great Post'}

function postForComment(posts, comment) {
  return posts.find((post) => {
    return post.id === comment.postId
  });
}

postForComment(posts, comment);


/*
 * every
 */
var computers = [
  { name: 'Apple', ram: 24},
  { name: 'Compag', ram: 4},
  { name: 'Acer', ram: 32}
]

computers.every((computer) => {
  return computer.ram >= 4;
});

computers.every((computer) => {
  return computer.ram > 16;
});

var names = [ "Adan", "Ben", "David", "Corey", "Alma", "Stephen", "Alex" ]
names.every((name) => {
  return name.length > 6
});

/*
 * some
 */
computers.some((computer) => {
  return computer.ram >= 4;
});

computers.some((computer) => {
  return computer.ram > 16;
});

names.some((name) => {
  return name.length > 6
});

/*
 * reduce
 */
var numbers = [10, 20, 30];
numbers.reduce((acc, num) => {
  return acc + num;
}, 0);

numbers.reduce((acc, num) => {
  return acc + num;
}, 100);

var primaryColors = [
  { color: 'red' },
  { color: 'blue' },
  { color: 'yellow' }
]
primaryColors.reduce((acc, color) => {
  acc.push(color.color);
  return acc;
}, [])
