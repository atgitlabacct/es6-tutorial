'use strict';

// Javascript classes and prototypical inheritance
// Note that when we use a class we are still using prototypical inheritance

// Old ES 6 way, when we define a function that is suppose to be instantiated
// it creates a OldCar {} object which things can be cloned from
function OldCar(options) {
    this.title = options.title
}

console.log("-- OldCar prototype")
OldCar // A function names OldCar
(new OldCar({title: "foo"})).__proto__ // Points to the OldCar {} object
(new OldCar({title: "foo"})).constructor //Points to the OldCar Function
(new (new OldCar({title: "foo"})).constructor({title: 'Bar'}).constructor) //Points to the OldCar Function

OldCar.prototype //Points to the OldCar {} object, this is a shared object


OldCar.prototype.drive = function () {
    return 'vroom';
}

console.log("-- OldCar prototype -- added a function to prototype")
OldCar.prototype

const oldCar = new OldCar({title: 'Focus'});
OldCar.prototype
oldCar.drive();

console.log("-- OldCar instance prototype")
// Nothing because this is not a function
// Only functions have a prototype
oldCar.prototype === undefined;
// The object has a __proto__, not a prototype
console.log("-- OldCar instance __proto__")
oldCar.__proto__
// So from above oldCar.__proto__ actuall points to the OldCar function's
// prototype method.

// Create a new constructor function
function OldToyota(options) {
    OldCar.call(this, options)
    this.color = options.color || 'Black';
}

// Create a new object to 'build' from so now OldToyota instances
// will go up the chain to an object that is a dup of OldCar.prototype
// which is an OldCar object with the drive method
// We assign the constructor function to OldToyota so we know what
// how the instances where built
OldToyota.prototype = Object.create(OldCar.prototype);
OldToyota.prototype.constructor = OldToyota

OldToyota.prototype.honk = function () {
    return 'beep';
}

const oldToyota = new OldToyota({color: 'Red', title: 'Daily Driver'});
oldToyota.drive();
oldToyota.honk();
oldToyota.__proto__

// -------------------------------------------------------------------
// Now with ES6 we no longer need to set functions on the prototype,
// manually assign prototypes and constructors.
export class NewCar {
    constructor({title}) {
        this.title = title
    }

    drive() {
        return 'vroom';
    }
}

console.log("-- NewCar prototype")
NewCar.prototype

const newCar = new NewCar({title: 'New Car'});
newCar
newCar.drive();
// Notice the output from these.  It matches the above after all the assigning
// of prototypes and constructors
newCar.prototype === undefined
newCar.__proto__
newCar.__proto__.constructor

// extends take care of all that bullshit about adding prototype
// and constructor
class NewToyota extends NewCar {
    constructor({title, color}) {
        super({title, color}); // available from extends
        this.color = color;
    }
    honk() {
        return 'beep';
    }
}

const newToyota = new NewToyota({title: 'Toyota', color: 'Red'})
newToyota.honk();

