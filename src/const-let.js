/*
 * var is no more!
 */

// var name = "Jane";
// var title = "Software Engineer"
// var hourlyWage = 40;

// const is a constant of course...doesn't change
const name = "Jane";

name = "Foo"; // Throws an error

let title = "Software Engineer";
title
title = "Sr. Software Engineer";
`${name} is a ${title}` // Template string/literal

/*
 * The benefits of ABOVE give us more context on what the variables are used for.
 * for example in the above I do not expect name to change, but I do expect
 * title to change
 */
