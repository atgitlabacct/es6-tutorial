'use strict';

/*
 * ES5 Code
 */
var expense = {
    type: 'Business',
    amount: '$45 USD'
}

// var type =  expense.type;
// var amount = expense.amount


// ES6

// We are not creating an object here.
// This is just declaring a variable each property for type, amount
//
// Note that the properties have to be identical.
const { type, amount, missing } = expense;

type
amount
missing === undefined // undefined


/*
 * Some ES5 code
 */
var savedFiled = {
    extension: 'jpg',
    name: 'repost',
    size: 14040
};

function fileSummaryOne(file) {
    return `The file ${file.name}.${file.extension} is size ${file.size}`;
}

fileSummaryOne(savedFiled);

// Some ES6 updated code using destructuring.
// Pulling off props for objects
function fileSummaryTwo({name, extension, size}, {color}) {
    return `The file ${name}.${extension} is size ${size}, color: ${color}`;
}

fileSummaryTwo(savedFiled, {color: 'blue'});

// Destructuring arrays
const companies =[
    {name: 'Google', location: ['Mountain View', 'Seatle']},
    {name: 'Facebook', location: ['Menlo Park']},
    {name: 'Uber', location: ['San Francisco']},
    {name: 'Udemy', location: ['San Francisco']},
    {name: 'Microsoft', location: ['Seatle']}
]

const [ one, {name, location}, ...rest ] = companies;
one
name
location
rest

// Destrucuring can be {} and [].  You can see that [] is array/positional and
// {} are used for object props.
// So recursion becomes interesting
function signup({password, username, email}) {
    console.log(`Create ${username}, ${password}, ${email}`)
}

const user = {
    username: 'myname',
    password: 'mypassword',
    email: 'myemail@example.com',
    dob: '1/1/1900',
    city: 'New York'
}

signup(user);

// Lets try an destructur in a a anon function
//
function myJoin(objs) {
    return objs.reduce((acc, {name}) => {
        if (acc === "") {
            return name;
        } else {
            return `${acc}, ${name}`;
        }
    }, "");
}

myJoin(companies);

const points = [
    [4,5],
    [10,1],
    [0,40]
];

points.map(([x,y]) => {
    return {x, y};
})

function double([head, ...tail], acc = []) {
    if (head === undefined) {
        return  acc;
    } else {
        acc.push(head * 2);
        return double(tail, acc)
    }
}

double([1,2,3])
