const secret = msg => () => msg;

const secretTwo = function(msg) {
    return function() { return msg; }
};

secret("Hello");
secret("Hello")();

secretTwo("Good Bye")
secretTwo("Good Bye")();
