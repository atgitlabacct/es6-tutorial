// -------------------------------------------------------------------
// Now with ES6 we no longer need to set functions on the prototype,
// manually assign prototypes and constructors.
'use strict';

class NewCar {
    constructor({title}) {
        this.title = title
    }

    beep() { return "beep, beep";
    }

    drive() {
        return 'vroom';
    }
}

exports.NewCar = NewCar;
