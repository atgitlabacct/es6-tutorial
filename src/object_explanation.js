'use strict';

/*
 * lets dive into js prototypical inheritance and object creation
 *
 * The first thing to imagine is a Tree.  An actual physical tree thats just
 * bare, no bark, no leaves, no branches.  We don't even know the type of wood
 * it is.  Really imagine a Tree in an abstract form.  A kind of 'fuzzy' tree.  I
 * imagine a tree that is changing to all differnt types with a shimmer or haze
 * as the outline, but it never really takes physical form.
 *
 * The above paragraph is how I imagine abstract objects.  The Function or class
 * that is used to add hard aspects of that tree is how a 'real' tree comes to
 * life and actual gets its physical properties or state.
 */

function Tree(options) {
    this.wood_type = options.wood_type;
}

Tree.prototype;

/*
 * The tree above is a good example of what I think of as a 'fuzzy' tree.  We
 * start to get an idea of a tree, but nothing really concrete.
 */

let fuzzyTree = new Tree({wood_type: 'hard'});
fuzzyTree
fuzzyTree.__proto__;
fuzzyTree.constructor;

fuzzyTree.color = "Green";
fuzzyTree;
fuzzyTree.__proto__;

Tree.prototype.bark = "Grey";
fuzzyTree.bark

/* We now created that fuzzy tree.  It doesn't do much, but there is an
 * idea of a tree.  This idea actually has some state.  The Function we used
 * was like the Hand of God reaching down and plucking a tree out of nothing.
 * God has an idea or blueprint of what a tree would be and that is what the
 * Function is.  Its the blueprint of a basic fuzzy tree.
 *
 * God is now tired of a fuzzy tree.  It has no color.  It has no bark.  It
 * doesn't really have much of anytihng.  So he decides to create a new type
 * of tree, but he still wants to use some aspects of his fuzzy tree.
 */

function MapleTree(options) {
    this.color = options.color;
    this.wood_type = options.wood_type;
    this.bark = options.bark || this.bark;
}

/*
 * Code decided he wanted to not touch the original fuzzy tree.  We cloned or created
 * a new fuzzy tree based on the previous one.
 */
MapleTree.prototype = Object.create(fuzzyTree);

let maple = new MapleTree({color: "Red", wood_type: 'Soft'});
maple.__proto__
maple.bark
maple.color
maple.wood_type

fuzzyTree.bark = "Brown";
maple.bark
fuzzyTree.bark

