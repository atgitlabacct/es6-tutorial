function createBookShop(inventory) {
    return {
        inventory, // normally would be inventory: inventory
        inventoryValue() { // normally inventoryValue: function() {
            return this.inventory.reduce((total, book) => {
                return total + book.price; 
            }, 0)
        },
        priceForTitle(title) {
            book = this.inventory.find((book) => {
                return book.title === title
            });
            return book.price;
        }
    };
}


const inventory = [
    { title: 'Harry Potter', price: 10 },
    { title: 'Eloquent Javascript', price: 15 }
]

const bookShop = createBookShop(inventory);
bookShop.inventoryValue();
bookShop.priceForTitle('Harry Potter');
bookShop.priceForTitle('Eloquent Javascript');


/*
 * Default function argument
 */
function doAjax(data, method = "GET") {
    console.log(`${method}ing ${JSON.stringify(data)}`)
}

doAjax({some: 'data'})
doAjax({some: 'data'}, 'POST')


class User {
    constructor(id) {
        this.id = id
    }
}

function generateId() {
    return Math.random() * 9999999;
}

function createAdminUser(user = new User(generateId())) {
    user.admin = true;

    return user;
}

user = createAdminUser();
