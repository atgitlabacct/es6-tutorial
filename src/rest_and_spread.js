'use strict';

function addNumbers(numbers) {
    return numbers.reduce((sum, number) => {
        return sum + number;
    }, 0)
}

addNumbers([1,2,3,4,5]);

/*
 * The rest operator (...)
 * Captures a list of arguments
 */
function addMultipleNumbers(...args) {
    return args.reduce((sum, arg) => {
        return sum + arg
    })
}

addMultipleNumbers(1,2,3,4,5,6,7,8,9);

// Decent example of usage of rest operator
const MathLibrary = {
    calculateProduct(...numbers) {
        console.log("DEPRECATED: Please use multiple")
        this.multiple(...numbers)
    },

    multiple(...args) {
        return args.reduce((sum, arg) => {
            return sum * arg;
        }, 1);
    }
}

MathLibrary.calculateProduct(2,2);
MathLibrary.multiple(2,2, 5, 1, 3);


/*
 * The spread operator (...)
 */
const defaultColors = ['red', 'green'];
const userFavoriteColors = ['orange', 'yellow'];


// First example flattens out the arrays
[...defaultColors, ...userFavoriteColors];
// Notice the difference without the dot
[...defaultColors, userFavoriteColors];

// One way without the spread, using concat
defaultColors.concat(userFavoriteColors);

const fallColors = ['fire red', 'fall orange'];

[...fallColors, ...defaultColors, ...userFavoriteColors, "blue"]

// To do the above with concat we need
defaultColors.concat(userFavoriteColors).concat(fallColors).concat('blue');
