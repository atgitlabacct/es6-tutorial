'use strict';

import { NewCar } from '../src/new_car.js';

const assert = require('assert');

describe('NewCar', () => {
    it('honks beep', () => {
        const newCar = new NewCar({title: 'My Car'});
        assert.equal(newCar.beep(), 'beep, beep');
    })
})
